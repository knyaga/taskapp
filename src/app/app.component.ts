import { Component } from '@angular/core';
import {Task} from "./models/Task.model";

@Component({
  moduleId: module.id,
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css']
})
export class AppComponent {
   private tasks: Task [] = [];
   private currentTask = new Task(null, false);

    addNewTask() {
      let task = new Task(this.currentTask.body, this.currentTask.completed);
      this.tasks.push(task);
      this.currentTask.body = null;
    }
}
