/**
 * Created by Otis on 17/01/17.
 */
export class Task {
  constructor(
    public body: string,
    public completed: boolean
  ){ }
}
