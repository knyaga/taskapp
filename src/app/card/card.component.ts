/**
 * Created by Otis on 17/01/17.
 */
import {Component, OnInit, Input} from '@angular/core';

import {Task} from '../models/Task.model';

@Component({
  moduleId: module.id,
  selector:'app-card',
  templateUrl:'card.component.html',
  styleUrls: ['card.component.css']
})
export class CardComponent {
  @Input() task: Task;

  toggleCompleted() {
    this.task.completed = !this.task.completed;
  }
}
